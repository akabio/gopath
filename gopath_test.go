package gopath_test

import (
	"testing"

	"gitlab.com/akabio/expect"
	"gitlab.com/akabio/gopath"
)

func TestMyself(t *testing.T) {
	pkgn, _ := gopath.Find("foo/bar/baz")
	expect.Value(t, "package path", pkgn).ToBe("gitlab.com/akabio/gopath/foo/bar/baz")

	pkgn, _ = gopath.Find("foo/bar/baz/..")
	expect.Value(t, "package path", pkgn).ToBe("gitlab.com/akabio/gopath/foo/bar")

	pkgn, _ = gopath.Find("foo/bar/baz.go")
	expect.Value(t, "package path", pkgn).ToBe("gitlab.com/akabio/gopath/foo/bar")

	_, err := gopath.Find("/x/y/z")
	expect.Value(t, "error", err).NotToBe(nil)
	expect.Value(t, "error", err.Error()).ToBe("could not find go.mod in parent path")
}

func TestRelativePath(t *testing.T) {
	pkgn, _ := gopath.RelativePath("foo/bar/baz")
	expect.Value(t, "package path", pkgn).ToBe("foo/bar/baz")

	pkgn, _ = gopath.RelativePath("../gopath/foo/bar/baz/..")
	expect.Value(t, "package path", pkgn).ToBe("foo/bar")

	pkgn, _ = gopath.RelativePath("foo/bar/baz.go")
	expect.Value(t, "package path", pkgn).ToBe("foo/bar/baz.go")

	_, err := gopath.RelativePath("/x/y/z")
	expect.Value(t, "error", err).NotToBe(nil)
	expect.Value(t, "error", err.Error()).ToBe("could not find go.mod in parent path")
}

func TestInvalidFormat(t *testing.T) {
	_, err := gopath.Find("testdata/invalidformat")
	expect.Value(t, "error", err).NotToBe(nil)
	expect.Value(t, "error", err.Error()).ToHavePrefix("could not read path from go.mod file '")
	expect.Value(t, "error", err.Error()).ToHaveSuffix("/testdata/invalidformat/go.mod'")
}
