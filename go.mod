module gitlab.com/akabio/gopath

go 1.15

require (
	gitlab.com/akabio/expect v0.2.0
	golang.org/x/mod v0.3.0
	gopkg.in/yaml.v2 v2.3.0 // indirect
)
