package gopath

import (
	"errors"
	"fmt"
	"io/ioutil"
	"path"
	"path/filepath"
	"strings"

	"golang.org/x/mod/modfile"
)

// Find will return the full package name of the provided folder.
func Find(p string) (string, error) {
	absp, err := filepath.Abs(p)
	if err != nil {
		return "", err
	}

	// in case the last part has a .go extension we assume the file is given
	// and the package path should omit that file.
	if strings.HasSuffix(filepath.Base(absp), ".go") {
		absp = filepath.Dir(absp)
	}
	return build(absp)
}

func build(p string) (string, error) {
	d, err := ioutil.ReadFile(filepath.Join(p, "go.mod"))
	if err != nil {
		if p == "/" {
			return "", errors.New("could not find go.mod in parent path")
		}
		ret, err := build(filepath.Dir(p))
		if err != nil {
			return "", err
		}
		return path.Join(ret, filepath.Base(p)), nil
	}
	mp := modfile.ModulePath(d)
	if mp == "" {
		return "", fmt.Errorf("could not read path from go.mod file '%v'", filepath.Join(p, "go.mod"))
	}
	return mp, nil
}

// RelativePath will return the relative path from the module root by probing for go.mod.
func RelativePath(p string) (string, error) {
	absp, err := filepath.Abs(p)
	if err != nil {
		return "", err
	}

	return buildRelPath(absp)
}

func buildRelPath(p string) (string, error) {
	_, err := ioutil.ReadFile(filepath.Join(p, "go.mod"))
	if err != nil {
		if p == "/" {
			return "", errors.New("could not find go.mod in parent path")
		}
		ret, err := buildRelPath(filepath.Dir(p))
		if err != nil {
			return "", err
		}
		return path.Join(ret, filepath.Base(p)), nil
	}
	// we found go.mod
	return "", nil
}
