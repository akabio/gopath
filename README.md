# gopath

Find out full package name of folder by probing for go.mod

## Example

```go
...
import (
  "gitlab.com/akabio/gopath"
  "fmt"
)
...
p, _ := gopath.Find("../test/foo/bar")
fmt.Println(p) // gitlab.com/akabio/test/foo/bar
...
```
